using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public static LevelManager instance;
    GameObject player;
    HealthManager playerHealthManager;
    [SerializeField] GameObject DeathScreen;
    [SerializeField] Doorway Doorway;
    bool IsPaused;

    List<GameObject> Enemies = new List<GameObject>();

    void Awake(){
        if(instance){
            Destroy(gameObject);
        } else {
            instance = this;
        }
    }

    void Start(){
        DeathScreen.SetActive(false);
        player = GameObject.FindGameObjectWithTag("Player");
        if(player.TryGetComponent<HealthManager>(out HealthManager healthManager))
        {
            playerHealthManager = healthManager;
            playerHealthManager.OnDeath += PlayerDeath;
        }
    }

    void OnDestroy(){
        playerHealthManager.OnDeath -= PlayerDeath;
    }

    void PlayerDeath(GameObject _){
        DeathScreen.SetActive(true);
    }


    public void Restart(){
        Scene ActiveScene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(ActiveScene.name);
    }

    public void AddEnemy(GameObject enemy){
        Enemies.Add(enemy);
        enemy.GetComponent<HealthManager>().OnDeath += OnEnemyDeath;
    }

    public void OnEnemyDeath(GameObject enemy){
        Enemies.Remove(enemy);
        enemy.GetComponent<HealthManager>().OnDeath -= OnEnemyDeath;
        if(IsLastEnemy()){
            Doorway.OpenDoorway();
        }
    }
    
    public void LoadNextScene(){
        int nextscene = SceneManager.GetActiveScene().buildIndex + 1;
        SceneManager.LoadScene(nextscene);
    }

    public void TogglePause(){
        IsPaused = !IsPaused;
        if(IsPaused){
            Time.timeScale = 0f;
        } else {
            Time.timeScale = 1f;
        }
    }
    
    bool IsLastEnemy(){
        return Enemies.Count == 0;
    }
}
