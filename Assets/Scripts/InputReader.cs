using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputReader : MonoBehaviour, Controls.IPlayerActions
{
    Controls controls;
    public Vector2 MovementValue {get; private set;}
    public event Action AttackEvent;
    public event Action MenuEvent;
    void Start()
    {
        controls = new Controls();
        controls.Player.SetCallbacks(this);
        controls.Player.Enable();
    }

    void OnDestroy(){
        controls.Player.Disable();
    }

    public void OnMove(InputAction.CallbackContext context)
    {
        MovementValue = context.ReadValue<Vector2>();
    }

    public void OnAttack(InputAction.CallbackContext context)
    {
        if(!context.performed){ return;}
        AttackEvent?.Invoke();
    }

    public void OnMenu(InputAction.CallbackContext context)
    {
        if(!context.performed){ return;}
        MenuEvent?.Invoke();
    }
}
