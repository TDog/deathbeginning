using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthManager : MonoBehaviour
{
    public event Action<GameObject> OnDeath;
    public event Action<float, float> OnHealthChanage;
    [SerializeField] float maxHealth = 100f;
    public float CurrentHealth {get; private set;}
    
    void Start(){
        Reset();
    }

    public void Reset(){
        CurrentHealth = maxHealth;
        OnHealthChanage?.Invoke(CurrentHealth, maxHealth);
    }

    public void TakeDamage(float damage){
        if(CurrentHealth > 0f){
            CurrentHealth -= damage;
        }
        if(CurrentHealth <= 0f){
            OnDeath?.Invoke(gameObject);
        }
        OnHealthChanage?.Invoke(CurrentHealth, maxHealth);
    }
}
