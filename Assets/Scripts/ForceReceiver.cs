using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceReceiver : MonoBehaviour
{
    float verticalVelocity;
    [SerializeField] Rigidbody rb;
    public Vector3 Movement => Vector3.up * verticalVelocity + impact;
    Vector3 impact = Vector3.zero;

    public void AddVerticalForce(float verticalForce){
        verticalVelocity += verticalForce * Time.deltaTime;
    }

    public void Knockback(Vector3 force){
        if(rb){
            rb.AddForce(force, ForceMode.Impulse);
        } else {
            impact += force;
        }
    }
}
