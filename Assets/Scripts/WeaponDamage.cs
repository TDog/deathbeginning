using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponDamage : MonoBehaviour
{
    [SerializeField] float damage = 20f;
    [SerializeField] float knockbackForce = 20f;
    [SerializeField] Collider myCollider;
    List<Collider> alreadyCollidedWith = new List<Collider>();
    [SerializeField] AudioSource audioSource;
    [SerializeField] AudioClip weaponHitClip;

    void OnEnable(){
        alreadyCollidedWith.Clear();
    }

    void OnTriggerEnter(Collider other){
        if(other == myCollider){ return;}
        
        if(alreadyCollidedWith.Contains(other)){ return; }
        
        alreadyCollidedWith.Add(other);
        
        if(other.TryGetComponent<HealthManager>(out HealthManager healthManager)){
            audioSource.PlayOneShot(weaponHitClip);
            healthManager.TakeDamage(damage);

            // knockback
            if(other.TryGetComponent<ForceReceiver>(out ForceReceiver forceReceiver))
            {
                Vector3 dir = other.transform.position - transform.position;
                Vector3 force = dir.normalized * knockbackForce;
                force.y = 0;
                forceReceiver.Knockback(force);
            }
        }
    }
}
