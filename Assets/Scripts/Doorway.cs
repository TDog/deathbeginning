using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Doorway : MonoBehaviour
{
    [SerializeField] GameObject DoorwayTrigger;
    [SerializeField] GameObject DoorwayBlocker;

    void Start(){
        DoorwayTrigger.SetActive(false);
        DoorwayBlocker.SetActive(true);
    }

    public void OpenDoorway(){
        DoorwayTrigger.SetActive(true);
        DoorwayBlocker.SetActive(false);
    }
}
