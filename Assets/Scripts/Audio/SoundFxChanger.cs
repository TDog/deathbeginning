using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundFxChanger : VolumeChanger
{
    void OnEnable(){
        soundManager.OnSoundFxVolumeChange += ChangeSound;
        ChangeSound(soundManager.CurrentSoundFxVolume);
    }

    void Ondestroy(){
        soundManager.OnSoundFxVolumeChange -= ChangeSound;
    }
}
