using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour
{
    public event Action<float> OnSoundFxVolumeChange;
    public event Action<float> OnMusicVolumeChange;
    [SerializeField] Slider musicSlider;
    [SerializeField] Slider soundFxSlider;
    public float CurrentSoundFxVolume {get; private set;}
    public float CurrentMusicVolume {get; private set;}

    void Start(){
        float startingVolume = .8f;
        ChangeSoundFxAudio(startingVolume);
        ChangeMusicAudio(startingVolume);
        musicSlider.value = startingVolume;
        soundFxSlider.value = startingVolume;
    }

    void ChangeSoundFxAudio(float newVolume){
        OnSoundFxVolumeChange?.Invoke(newVolume);
        CurrentSoundFxVolume = newVolume;
    }

    void ChangeMusicAudio(float newVolume){
        OnMusicVolumeChange?.Invoke(newVolume);
        CurrentMusicVolume = newVolume;
    }

    public void SliderMusicChange(){
        ChangeMusicAudio(musicSlider.value);
    }

    public void SliderSoundFxChange(){
        ChangeSoundFxAudio(soundFxSlider.value);
    }
}
