using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolumeChanger : MonoBehaviour
{
    AudioSource audioSource;
    [HideInInspector] public SoundManager soundManager;
    float defaultVolume;
    public float CurrentVolume {get; private set;}

    void Awake(){
        audioSource = GetComponent<AudioSource>();
        defaultVolume = audioSource.volume;
        soundManager = GameObject.FindObjectOfType<SoundManager>();
    }

    public void ChangeSound(float newVolume){
        audioSource.volume = newVolume * defaultVolume;
    }
}
