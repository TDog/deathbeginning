using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicFxChanger : VolumeChanger
{
    void OnEnable(){
        soundManager.OnMusicVolumeChange += ChangeSound;
        ChangeSound(soundManager.CurrentMusicVolume);
    }

    void OnDisable(){
        soundManager.OnMusicVolumeChange -= ChangeSound;
    }
}
