using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioMenu : MonoBehaviour
{
    [SerializeField] InputReader inputReader;
    [SerializeField] GameObject Menu;
    void Start(){
        inputReader.MenuEvent += ToggleMenu;
    }

    public void ToggleMenu(){
        // this is for start scene and end scene since it doesn't have a level manager
        if(LevelManager.instance)
            LevelManager.instance.TogglePause();

        Menu.SetActive(!Menu.activeInHierarchy);
    }
}
