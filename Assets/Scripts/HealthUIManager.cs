using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthUIManager : MonoBehaviour
{
    [SerializeField] Slider HealthBarSlider;
    [SerializeField] Gradient gradient;
    [SerializeField] Image fill;
    [SerializeField] HealthManager healthManager;
    [SerializeField] bool billboard;
    Transform cam;

    void Start(){
        healthManager.OnHealthChanage += HealthChange;
        HealthBarSlider.value = 1;
        fill.color = gradient.Evaluate(1f);
        cam = Camera.main.transform;
    }

    void LateUpdate(){
        if(!billboard){return;}

        transform.LookAt(transform.position + cam.forward);
    }

    void Destroy(){
        healthManager.OnHealthChanage -= HealthChange;
    }

    void HealthChange(float currentHealth, float maxHealth){
        float healthPercentage = currentHealth / maxHealth;
        HealthBarSlider.value = healthPercentage;
        fill.color = gradient.Evaluate(healthPercentage);
    }
}
