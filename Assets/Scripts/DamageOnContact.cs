using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageOnContact : MonoBehaviour
{
    [SerializeField] float damage = 5f;
    [SerializeField] float EverySec = .5f;
    float lastTime = 0f;

    void OnTriggerStay(Collider collider){
        if(collider.gameObject.TryGetComponent<HealthManager>(out HealthManager healthManager))
        {
            if(Time.timeSinceLevelLoad - lastTime > EverySec ){
                lastTime = Time.timeSinceLevelLoad;
                healthManager.TakeDamage(damage);
            }
        }
    }
}
