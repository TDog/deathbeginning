using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPatrolState : EnemyBaseState
{
    Vector3 walkPoint;
    bool walkPointSet;
    public EnemyPatrolState(EnemyStateMachine stateMachine) : base(stateMachine)
    {
    }

    public override void Enter()
    {
        Debug.Log("Patrolling");
    }

    public override void Exit()
    {
    }

    public override void Tick(float deltaTime)
    {
        Patrol();
        if(stateMachine.IsPlayerInSightRange()){
            stateMachine.SwitchState(new EnemyChaseState(stateMachine));
        }
    }

    void RandomWalkPoint(){
        float randomZ = Random.Range(-stateMachine.WalkPointRange, stateMachine.WalkPointRange);
        float randomX = Random.Range(-stateMachine.WalkPointRange, stateMachine.WalkPointRange);

        walkPoint = new Vector3(stateMachine.transform.position.x + randomX, stateMachine.transform.position.y, stateMachine.transform.position.z + randomZ);
        if(Physics.Raycast(walkPoint, -stateMachine.transform.up, 2f, stateMachine.GroundMask)){
            walkPointSet = true;
        }
    }

    void Patrol(){
        if(!walkPointSet) RandomWalkPoint();

        if(walkPointSet){
            stateMachine.NavMeshAgent.SetDestination(walkPoint);
        }

        Vector3 distanceToWalkPoint = stateMachine.transform.position - walkPoint;

        if(distanceToWalkPoint.magnitude < 1f){ walkPointSet = false;}
    }

    public override void DrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(stateMachine.transform.position, stateMachine.SightRange);
    }

    public override void FixedTick(float fixedDeltaTime)
    {
    }
}
