using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyChaseState : EnemyBaseState
{
    public EnemyChaseState(EnemyStateMachine stateMachine) : base(stateMachine)
    {
    }

    public override void DrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(stateMachine.transform.position, stateMachine.AttackRange);
    }

    public override void Enter()
    {
        Debug.Log("Chasing");
    }

    public override void Exit()
    {
    }

    public override void FixedTick(float fixedDeltaTime)
    {
    }

    public override void Tick(float deltaTime)
    {
        stateMachine.NavMeshAgent.destination = stateMachine.Player.position;

        if(stateMachine.IsPlayerInAttackRange()){
            stateMachine.SwitchState(new EnemyAttackState(stateMachine));
        } else if(!stateMachine.IsPlayerInSightRange()){
            stateMachine.SwitchState(new EnemyPatrolState(stateMachine));
        }
    }
}
