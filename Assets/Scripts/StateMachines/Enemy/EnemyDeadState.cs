using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDeadState : EnemyBaseState
{
    private readonly int IsDeadHash = Animator.StringToHash("IsDead");
    public EnemyDeadState(EnemyStateMachine stateMachine) : base(stateMachine)
    {
    }

    public override void DrawGizmos()
    {
    }

    public override void Enter()
    {
        stateMachine.NavMeshAgent.SetDestination(stateMachine.transform.position);
        stateMachine.NavMeshAgent.isStopped = true;
        stateMachine.Animator.SetBool(IsDeadHash, true);
        stateMachine.AudioSource.Stop();
    }

    public override void Exit()
    {
    }

    public override void FixedTick(float fixedDeltaTime)
    {
    }

    public override void Tick(float deltaTime)
    {
    }
}
