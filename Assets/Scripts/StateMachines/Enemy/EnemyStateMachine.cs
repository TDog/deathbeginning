using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyStateMachine : StateMachine
{
    [field: Header("Scripts")]
    [field: SerializeField] public HealthManager HealthManager {get; private set;}
    [field: SerializeField] public ForceReceiver ForceReceiver {get; private set;}
    [field: SerializeField] public Animator Animator {get; private set;}
    [field: SerializeField] public NavMeshAgent NavMeshAgent {get; private set;}
    [field: SerializeField] public AudioSource AudioSource {get; private set;}

    [field: Header("Ranges")]
    [field: SerializeField] public float AttackRange {get; private set;}
    [field: SerializeField] public float SightRange {get; private set;}
    [field: SerializeField] public float WalkPointRange {get; private set;}

    [field: Header("Attack")]
    [field: SerializeField] public float TimeBetweenAttacks {get; private set;}

    [field: Header("Rotation")]
    [field: SerializeField] public float RotationDamping {get; private set;}

    [field: Header("Masks")]
    [field: SerializeField] public LayerMask PlayerMask {get; private set;}
    [field: SerializeField] public LayerMask GroundMask {get; private set;}

    [HideInInspector] public float TimeSinceLastAttack = Mathf.Infinity;
    public readonly int AttackHash = Animator.StringToHash("Attack");
    public Transform Player {get; private set;}

    void Start(){
        LevelManager levelManager = LevelManager.instance;
        levelManager.AddEnemy(gameObject);
        Player = GameObject.FindGameObjectWithTag("Player").transform;
        SwitchState(new EnemyPatrolState(this));
        HealthManager.OnDeath += Death;
    }

    public override void Update()
    {
        base.Update();
        TimeSinceLastAttack += Time.deltaTime;
    }

    void Death(GameObject _){
        SwitchState(new EnemyDeadState(this));
    }

    void OnDestroy(){
        HealthManager.OnDeath -= Death;
    }

    public bool IsPlayerInSightRange(){
        return Physics.CheckSphere(transform.position, SightRange, PlayerMask);
    }

    public bool IsPlayerInAttackRange(){
        return Physics.CheckSphere(transform.position, AttackRange, PlayerMask);
    }

    public float GetNormalizedTimeForAnimation(int layerIndex, string tag)
    {
        AnimatorStateInfo currentInfo = Animator.GetCurrentAnimatorStateInfo(layerIndex);
        AnimatorStateInfo nextInfo = Animator.GetCurrentAnimatorStateInfo(layerIndex);

        if(Animator.IsInTransition(layerIndex) && nextInfo.IsTag(tag)){
            return nextInfo.normalizedTime;
        }
        else if(Animator.IsInTransition(layerIndex) && currentInfo.IsTag(tag)){
            return currentInfo.normalizedTime;
        }
        else {
            return 0f;
        }
    }

    public void LerpLookAt(Vector3 pos, float fixedDeltaTime){
        Vector3 dir = pos - transform.position;

        Quaternion toRotation = Quaternion.FromToRotation(transform.position, dir);

        transform.rotation = Quaternion.Lerp(
            transform.rotation, 
            toRotation,
            fixedDeltaTime * RotationDamping);
    }

    public bool CanAttack(){
        if(TimeSinceLastAttack > TimeBetweenAttacks){
            return true;
        }
        return false;
    }
}
