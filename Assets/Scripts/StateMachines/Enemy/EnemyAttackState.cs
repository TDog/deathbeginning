using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttackState : EnemyBaseState
{
    string attackTagString = "Attack";
    bool hasAttacked;

    public EnemyAttackState(EnemyStateMachine stateMachine) : base(stateMachine)
    {
    }

    public override void DrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(stateMachine.transform.position, stateMachine.AttackRange);
    }

    public override void Enter()
    {
        stateMachine.NavMeshAgent.SetDestination(stateMachine.transform.position);
        AttackPlayer();
    }

    public override void Exit()
    {
    }

    public override void Tick(float deltaTime)
    {
        if (!stateMachine.IsPlayerInAttackRange())
        {
            stateMachine.SwitchState(new EnemyChaseState(stateMachine));
            return;
        }
        AttackPlayer();
        NextAttack();
    }

    public override void FixedTick(float fixedDeltaTime)
    {
        stateMachine.LerpLookAt(stateMachine.Player.position, fixedDeltaTime);
        Debug.Log("Look");
    }

    private void NextAttack()
    {
        if(!hasAttacked) {return;}
        float normalizedTime = stateMachine.GetNormalizedTimeForAnimation(0, attackTagString);
        if(normalizedTime > 1f){
            stateMachine.SwitchState(new EnemyAttackState(stateMachine));
        }
    }

    private void AttackPlayer(){
        if(!stateMachine.CanAttack() && !hasAttacked){return;}
        hasAttacked = true;
        stateMachine.TimeSinceLastAttack = 0f;
        stateMachine.Animator.SetTrigger(stateMachine.AttackHash);
    }
}
