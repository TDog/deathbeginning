using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlayerBaseState : State
{
    protected PlayerStateMachine stateMachine;

    public PlayerBaseState(PlayerStateMachine stateMachine){
        this.stateMachine = stateMachine;
    }

    protected void Move(Vector3 motion, float deltaTime){
        stateMachine.CharacterController.Move((motion + stateMachine.ForceReceiver.Movement) * deltaTime);
    }

    protected void FaceMovementDirection(Vector3 movement, float deltaTime)
    {
        stateMachine.transform.rotation = Quaternion.Lerp(
            stateMachine.transform.rotation, 
            Quaternion.LookRotation(movement), 
            deltaTime * stateMachine.RotationDamping);
    }

    protected void NormalMovement(float deltaTime){
        Vector2 inputMovement = stateMachine.InputReader.MovementValue;
        Vector3 movement = new Vector3(inputMovement.x, 0f, inputMovement.y);

        Move(movement * stateMachine.MovementSpeed, deltaTime);
        if(movement == Vector3.zero){
            stateMachine.Animator.SetFloat(stateMachine.RunSpeedHash, 0f, stateMachine.AnimatorDampTime, deltaTime);
            return;
        }
        FaceMovementDirection(movement, deltaTime);
        stateMachine.Animator.SetFloat(stateMachine.RunSpeedHash, 1f, stateMachine.AnimatorDampTime, deltaTime);
    }
}
