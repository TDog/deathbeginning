using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerNormalState : PlayerBaseState
{
    public PlayerNormalState(PlayerStateMachine stateMachine) : base(stateMachine)
    {
    }

    public override void Enter()
    {
        stateMachine.InputReader.AttackEvent += OnAttack;
    }

    public override void Exit()
    {
        stateMachine.InputReader.AttackEvent -= OnAttack;
    }

    public override void Tick(float deltaTime)
    {
        NormalMovement(deltaTime);
    }

    void OnAttack(){
        stateMachine.SwitchState(new PlayerAttackingState(stateMachine));
    }

    public override void DrawGizmos()
    {
    }

    public override void FixedTick(float fixedDeltaTime)
    {
    }
}
