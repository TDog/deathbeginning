using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttackingState : PlayerBaseState
{
    int upperBody;
    string attackTagString = "Attack";
    public PlayerAttackingState(PlayerStateMachine stateMachine) : base(stateMachine)
    {
    }

    public override void DrawGizmos()
    {
    }

    public override void Enter()
    {
        upperBody = stateMachine.Animator.GetLayerIndex("UpperBody");
        stateMachine.Animator.SetLayerWeight(upperBody, 0.8f);
        stateMachine.Animator.SetTrigger(stateMachine.AttackHash);
        stateMachine.AudioSource.PlayOneShot(stateMachine.WeaponSwoosh);
        stateMachine.SwordTrail.SetActive(true);
    }

    public override void Exit()
    {
        stateMachine.Animator.SetLayerWeight(upperBody, 0f);
        stateMachine.SwordTrail.SetActive(false);
    }

    public override void FixedTick(float fixedDeltaTime)
    {
    }

    public override void Tick(float deltaTime)
    {
        NormalMovement(deltaTime);
        float normalizedTime = GetNormalizedTime();
        if(normalizedTime > 1f){
            stateMachine.SwitchState(new PlayerNormalState(stateMachine));
        }
    }

    float GetNormalizedTime()
    {
        AnimatorStateInfo currentInfo = stateMachine.Animator.GetCurrentAnimatorStateInfo(upperBody);
        AnimatorStateInfo nextInfo = stateMachine.Animator.GetCurrentAnimatorStateInfo(upperBody);

        if(stateMachine.Animator.IsInTransition(upperBody) && nextInfo.IsTag(attackTagString)){
            return nextInfo.normalizedTime;
        }
        else if(!stateMachine.Animator.IsInTransition(upperBody) && currentInfo.IsTag(attackTagString)){
            return currentInfo.normalizedTime;
        }
        else {
            return 0f;
        }
    }
}
