using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStateMachine : StateMachine
{
    [field: Header("Scripts")]
    [field: SerializeField] public InputReader InputReader {get; private set;}
    [field: SerializeField] public CharacterController CharacterController {get; private set;}
    [field: SerializeField] public ForceReceiver ForceReceiver {get; private set;}
    [field: SerializeField] public HealthManager HealthManager {get; private set;}
    [field: SerializeField] public Animator Animator {get; private set;}
    [field: SerializeField] public AudioSource AudioSource {get; private set;}

    [field: Header("Movement")]
    [field: SerializeField] public float MovementSpeed {get; private set;}
    [field: SerializeField] public float RotationDamping {get; private set;}

    [field: Header("Weapon")]
    [field: SerializeField] public AudioClip WeaponSwoosh {get; private set;}
    [field: SerializeField] public GameObject SwordTrail {get; private set;}
    public readonly int RunSpeedHash = Animator.StringToHash("RunSpeed");
    public readonly int AttackHash = Animator.StringToHash("Attack");
    public readonly float AnimatorDampTime = 0.1f;
    float verticalVelocity;

    void Start(){
        SwitchState(new PlayerNormalState(this));
        HealthManager.OnDeath += Death;
    }

    public override void Update()
    {
        base.Update();
        Gravity();
    }

    void Gravity(){
        if(verticalVelocity < 0f && CharacterController.isGrounded){
            verticalVelocity = Physics.gravity.y * Time.deltaTime;
        } else {
            verticalVelocity += Physics.gravity.y * Time.deltaTime;
        }
        ForceReceiver.AddVerticalForce(verticalVelocity);
    }

    void Death(GameObject _){
        SwitchState(new PlayerDeadState(this));
    }

    void OnDestroy(){
        HealthManager.OnDeath -= Death;
    }
}
