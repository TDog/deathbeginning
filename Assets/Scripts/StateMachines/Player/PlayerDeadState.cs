using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDeadState : PlayerBaseState
{
    private readonly int IsDeadHash = Animator.StringToHash("IsDead");

    public PlayerDeadState(PlayerStateMachine stateMachine) : base(stateMachine)
    {
    }

    public override void DrawGizmos()
    {
    }

    public override void Enter()
    {
        stateMachine.Animator.SetBool(IsDeadHash, true);
    }

    public override void Exit()
    {
    }

    public override void FixedTick(float fixedDeltaTime)
    {
    }

    public override void Tick(float deltaTime)
    {
    }
}
