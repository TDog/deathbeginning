using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine : MonoBehaviour
{
    private State currentState;

    public virtual void Update(){
        currentState?.Tick(Time.deltaTime);
    }

    public virtual void FixedUpdate(){
        currentState?.FixedTick(Time.fixedDeltaTime);
    }

    public void SwitchState(State newState){
        currentState?.Exit();
        currentState = newState;
        currentState?.Enter();
    }

    void OnDrawGizmosSelected(){
        currentState?.DrawGizmos();
    }
}
