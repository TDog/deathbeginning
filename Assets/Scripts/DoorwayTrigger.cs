using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorwayTrigger : MonoBehaviour
{
        LevelManager levelManager;
    void Start(){
        levelManager = LevelManager.instance;
    }

    void OnTriggerEnter(Collider collider){
        if(collider.CompareTag("Player")){
            levelManager.LoadNextScene();
        }
    }
}
