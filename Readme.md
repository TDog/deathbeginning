# Sir Nok: Escape From Hell

Was created for the [GameDev.tv Jam 2022](https://itch.io/jam/gamedevtv-jam-2022/community)
[Itch.io Page For the Game](https://tdog.itch.io/sir-nok-escape-from-hell)

## Attributions

### Assets Required to build from source

* Textmesh pro Essential Resources - not sure of the license whether I could included it in the source

### 3D Models

* Player Model/Animations/Sword/Demon [Quaternius](https://quaternius.com/) under CC0 license

### Audio

* Sword Swosh is a modified clip from [Freesound by qubodup](https://freesound.org/people/qubodup/sounds/60023/) Under CC0 License

* Sword Hit is modified clip from [Freesound by Eponn](https://freesound.org/people/Eponn/sounds/547042/) Under CC0 License

* Demon Bite Hit is a modified clip from [Freesound by v23](https://freesound.org/people/v23/sounds/589149/) Under CC0 License

* Lava Loop is a modified clip from [Freesound by Audionautics](https://freesound.org/people/Audionautics/sounds/133901/) License: Attribution 3.0
